﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OilAndGasProcessor.DataModeling;
using System.Linq;

namespace OilAndGasProcessorTests
{
    [TestClass]
    public class CalculatorTest
    {
        private readonly Cell inputCell = new Cell { A = 5, B = 6, C = 7, D = 8, Lateral = 2 };

        [TestMethod]
        public void ZeroDividingCellReturnsSameCell()
        {
            var cellsOfDividedCell = inputCell.DivideCell(0).ToList();

            Assert.AreEqual(cellsOfDividedCell.First(), inputCell);
        }

        [TestMethod]
        public void OneDividingCellReturnsCorrectResult()
        {
            var cellsOfDividedCell = inputCell.DivideCell(1).ToList();

            double expectedCp = 6.5; 

            Assert.AreEqual(cellsOfDividedCell.Count, 4);
            Assert.IsTrue(cellsOfDividedCell.Where(x => (x.A == 5) && (x.B == 5.5) && (x.C == expectedCp) && (x.D == 6.5) && (x.Lateral == 1)).Any());
            Assert.IsTrue(cellsOfDividedCell.Where(x => (x.A == 5.5 && (x.B == 6) && (x.C == 6.5) && (x.D == expectedCp) && (x.Lateral == 1))).Any());
            Assert.IsTrue(cellsOfDividedCell.Where(x => (x.A == expectedCp) && (x.B == 6.5) && (x.C == 7) && (x.D == 7.5) && (x.Lateral == 1)).Any());
            Assert.IsTrue(cellsOfDividedCell.Where(x => (x.A == 6.5) && (x.B == expectedCp) && (x.C == 7.5) && (x.D == 8) && (x.Lateral == 1)).Any());
        }

        
    }
}
